package com.zephr.examplelogin.utils

import java.util.*

enum class AuthProvider {
    GOOGLE,
    APPLE,
    FACEBOOK,
    LINKEDIN,
    TWITTER,
    MICROSOFT;

    fun lowerCaseName(): String {
        return name.toLowerCase(Locale.ROOT)
    }
}