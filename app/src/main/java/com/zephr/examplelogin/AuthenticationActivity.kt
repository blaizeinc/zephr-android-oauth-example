package com.zephr.examplelogin

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.browser.customtabs.CustomTabsIntent
import androidx.constraintlayout.widget.ConstraintLayout
import io.ktor.client.request.*
import io.ktor.client.statement.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class AuthenticationActivity : AppCompatActivity() {

    private var stateKey = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authentication)
        val provider = intent.getStringExtra("provider")

        if (provider.isNullOrEmpty()) { finish() }

        val authUrl = "${getString(R.string.zephr_cdn_url)}/blaize/oauth/$provider?client_type=android"

        Log.d("URI", Uri.parse(authUrl).toString())

        val builder = CustomTabsIntent.Builder()
        val customTabsIntent = builder.build()
        customTabsIntent.launchUrl(this, Uri.parse(authUrl))
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        val data = intent?.data
        when (data?.getQueryParameter("status")) {
            "success" -> {
                Log.d("AUTH", "Login successful")
                saveUserDetail(
                    getString(R.string.session_id_key),
                    data.getQueryParameter("session_id") ?: ""
                )
                backToMainActivity()
            }
            "partial" -> {
                Log.d("AUTH", "Partial registration successful")
                stateKey = data.getQueryParameter("state_key") ?: ""
            }
            "failure" -> {
                Log.e("AUTH", "Auth response error")
                Log.d("AUTH", data.getQueryParameter("message") ?: "")
                val statusText: TextView = findViewById(R.id.statusText)
                statusText.text = getString(R.string.error_state)
                findViewById<ConstraintLayout>(R.id.completeRegInputs).visibility = View.GONE
            }
            else -> {
                Log.e("AUTH", "Unknown auth response status")
                val statusText: TextView = findViewById(R.id.statusText)
                statusText.text = getString(R.string.error_state)
                findViewById<ConstraintLayout>(R.id.completeRegInputs).visibility = View.GONE
            }
        }

        val completeRegButton: Button = findViewById(R.id.completeRegButton)

        completeRegButton.setOnClickListener {
            val firstName: EditText = findViewById(R.id.editTextTextPersonName)

            GlobalScope.launch {
                val sessionId = completeRegistration(stateKey, mapOf("first-name" to firstName.text.toString()))
                if (!sessionId.isNullOrEmpty()) {
                    saveUserDetail(getString(R.string.session_id_key), sessionId)
                    backToMainActivity()
                }
            }
        }
    }

    private fun backToMainActivity() {
        val mainActivity = Intent(baseContext, MainActivity::class.java)
        mainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        mainActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        baseContext.startActivity(mainActivity)
    }

    private fun saveUserDetail(key: String, value: String) {
        val userDetails = getSharedPreferences(getString(R.string.user_details_key), MODE_PRIVATE) ?: return
        with(userDetails.edit()) {
            putString(key, value)
            apply()
        }
    }

    private suspend fun completeRegistration(token: String, attributes: Map<String, String>): String? {

        try {
            val response: HttpResponse = MainActivity.client.post {
                url("${getString(R.string.zephr_cdn_url)}/blaize/register")
                headers {
                    append("Content-Type", "application/json")
                    append("Accept", "application/json")
                }
                body = mapOf(
                    "identifiers" to emptyMap(),
                    "validators" to mapOf("token_exchange" to token),
                    "attributes" to attributes
                )
            }
            Log.d("REGISTER", response.toString())
            val cookie = response.headers.getAll("Set-Cookie")?.joinToString(";") ?: ""
            return Regex("blaize_session=([0-9a-zA-Z_\\-]+)").find(cookie)?.groupValues?.get(1) ?: ""
        } catch (e: Exception) {
            Log.e("REGISTER", e.toString())
            return null
        }
    }
}