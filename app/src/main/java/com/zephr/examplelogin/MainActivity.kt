package com.zephr.examplelogin

import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.zephr.examplelogin.utils.AuthProvider
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.features.logging.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class MainActivity : AppCompatActivity() {

    companion object {
        val client = HttpClient(CIO) {
            followRedirects = false
            install(JsonFeature) {
                serializer = KotlinxSerializer(kotlinx.serialization.json.Json {
                    prettyPrint = true
                    isLenient = true
                })
            }
            install(Logging) {
                logger = object : Logger {
                    override fun log(message: String) {
                        Log.v("KTOR", message)
                    }
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        val signInButtons: ConstraintLayout = findViewById(R.id.signInButtons)
        val googleSignIn: Button = findViewById(R.id.googleButton)
        val facebookSignIn: Button = findViewById(R.id.facebookButton)
        val appleSignIn: Button = findViewById(R.id.appleButton)
        val signOutButton: Button = findViewById(R.id.signOutButton)
        val statusText: TextView = findViewById(R.id.statusText)

        googleSignIn.setOnClickListener {
            Log.d("LOGIN", "Google")
            launchAuthenticationActivity(AuthProvider.GOOGLE)
        }

        facebookSignIn.setOnClickListener {
            Log.d("LOGIN", "Facebook")
            launchAuthenticationActivity(AuthProvider.FACEBOOK)
        }

        appleSignIn.setOnClickListener {
            Log.d("LOGIN", "Apple")
            launchAuthenticationActivity(AuthProvider.APPLE)
        }

        signOutButton.setOnClickListener {
            val sessionIdKey = getString(R.string.session_id_key)
            GlobalScope.launch {
                logout(getUserDetail(sessionIdKey))
                saveUserDetail(sessionIdKey, "")
                finish()
                startActivity(this@MainActivity.intent)
            }
        }

        val sessionId = getUserDetail(getString(R.string.session_id_key))

        if (sessionId.isEmpty()) {
            statusText.text = getString(R.string.signed_out)
            signInButtons.visibility = View.VISIBLE
            signOutButton.visibility = View.GONE
        } else {
            statusText.text = getString(R.string.signed_in, sessionId)
            signInButtons.visibility = View.GONE
            signOutButton.visibility = View.VISIBLE
        }
    }

    private fun launchAuthenticationActivity(provider: AuthProvider) {
        val intent = Intent(this, AuthenticationActivity::class.java)
        intent.putExtra("provider", provider.lowerCaseName())
        intent.addFlags(FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }

    private fun getUserDetail(key: String): String {
        return getSharedPreferences(getString(R.string.user_details_key), MODE_PRIVATE)
            .getString(key, "") ?: ""
    }

    private fun saveUserDetail(key: String, value: String) {
        val userDetails = getSharedPreferences(getString(R.string.user_details_key), MODE_PRIVATE) ?: return
        with(userDetails.edit()) {
            putString(key, value)
            apply()
        }
    }

    private suspend fun logout(sessionId: String) {

        try {
            val response: HttpResponse = client.post {
                url("${getString(R.string.zephr_cdn_url)}/blaize/logout")
                header("Cookie", "blaize_session=$sessionId")
            }
            Log.d("LOGOUT", response.toString())
        } catch (e: Exception) {
            Log.e("LOGOUT", e.toString())
        }
    }
}